package automic.online.jobdistributors;


//import ij.IJ;
import net.imagej.ImageJ;

import org.scijava.command.Command;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;

import automic.online.jobs.Job_Default;
import automic.online.jobs.common.Job_AutofocusInit_Commander;
import automic.online.jobs.common.Job_RecordFinish;
import automic.online.jobs.frap.Job_GetMultipleBleachRois_Script_Commander;


@Plugin(type = Command.class, headless = true, menuPath ="Plugins>Auto Mic Tools>Applications>Photomanipulation>AF LowZoom and PA - Script")
public class JobDistributor_MicroscopeCommander_AF_Photomanipulation_Script extends JobDistributor_Abstract implements Command {
	

	@Parameter
	ImageJ ij2;

	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_Commander.class,				"AF--",  "AF",	true);
		super.addImageJob(Job_GetMultipleBleachRois_Script_Commander.class,	"Selection--", "Selection",	true);
		super.addImageJob(Job_RecordFinish.class,							"PA--", "PA",	true);

	}
	
	@Override
	public void run() {
		Job_Default.ij2=ij2;
		this.run("");
		
	}

	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:\\tempDat\\AutoMic-test";
		this.setGeneralOptions(searchPath, true, false);
	}

	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(final String... args) {
		
		final ImageJ ij = new ImageJ();
		ij.launch(args);

		// Launch our "Hello World" command right away.
		ij.command().run(JobDistributor_MicroscopeCommander_AF_Photomanipulation_Script.class, true);

	}
	
}
