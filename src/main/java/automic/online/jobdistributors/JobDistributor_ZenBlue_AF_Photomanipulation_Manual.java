package automic.online.jobdistributors;


import ij.IJ;
import ij.ImageJ;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import automic.online.jobs.common.Job_AutofocusInit_ZenBlue;
import automic.online.jobs.common.Job_RecordFinish;
import automic.online.jobs.frap.Job_GetMultipleManualBleachRois_ZenBlue;
import automic.table.TableModel;
import automic.table.TableProcessor;

@Deprecated
public class JobDistributor_ZenBlue_AF_Photomanipulation_Manual extends JobDistributor_Abstract implements PlugIn {
	

	
	@Override
	protected void fillJobList(){
		
		
		
		super.addImageJob(Job_AutofocusInit_ZenBlue.class,					"AF--",  "AF",	true);
		super.addImageJob(Job_GetMultipleManualBleachRois_ZenBlue.class,	"Selection--", "Selection",	true);
		super.addImageJob(Job_RecordFinish.class,							"PA--", "PA",	true);

	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");
		
		outTbl.addFileColumns("AF", "IMG");
		outTbl.addFileColumns("Selection", "IMG");
		outTbl.addFileColumns("PA", "IMG");
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		TableProcessor tProcessor=new TableProcessor(_tModel);
		return tProcessor;
	}

	
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}


	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:\\tempDat\\AutoMic-test";
		this.setGeneralOptions(searchPath, true, false);
	}
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = JobDistributor_ZenBlue_AF_Photomanipulation_Manual.class;
		//String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		//String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		//System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
		//DEBUG=true;
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		// run the plugin
		
		//exrerimentFolderPath="D:/tempDat/AutoFRAP_test";
		//showDemo=true;
		//pnum=1;
		//setDebugConfiguration();
		

		//IJ.runPlugIn(clazz.getName(),"Debug run");
		IJ.runPlugIn(clazz.getName(),"");

	}
	
}
