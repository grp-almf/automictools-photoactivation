package automic.online.jobdistributors;


import net.imagej.ImageJ;

import org.scijava.command.Command;
import org.scijava.plugin.Plugin;

import automic.online.jobs.common.Job_AutofocusInit_Commander;
import automic.online.jobs.common.Job_RecordFinish;
import automic.online.jobs.frap.Job_GetMultipleManualBleachRois_Commander;

@Plugin(type = Command.class, headless = true, menuPath ="Plugins>Auto Mic Tools>Applications>Photomanipulation>AF LowZoom and PA - Manual")
public class JobDistributor_MicroscopeCommander_AF_Photomanipulation_Manual extends JobDistributor_Abstract implements Command {
	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_Commander.class,				"AF--",  "AF",	true);
		super.addImageJob(Job_GetMultipleManualBleachRois_Commander.class,	"Selection--", "Selection",	true);
		super.addImageJob(Job_RecordFinish.class,							"PA--", "PA",	true);

	}

	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:\\tempDat\\AutoMic-test";
		this.setGeneralOptions(searchPath, true, false);
	}

	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(final String... args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);

		ij.command().run(JobDistributor_MicroscopeCommander_AF_Photomanipulation_Segmentation.class, true);

	}
	
}
