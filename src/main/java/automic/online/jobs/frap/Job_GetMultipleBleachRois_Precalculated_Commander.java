package automic.online.jobs.frap;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.table.TableModel;
import automic.utils.ArrIndUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import net.imagej.ImageJ;
//import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import loci.plugins.in.ImporterOptions;
import mpicbg.imglib.algorithm.fft.PhaseCorrelation;
import mpicbg.imglib.algorithm.fft.PhaseCorrelationPeak;
import mpicbg.imglib.image.ImagePlusAdapter;


public class Job_GetMultipleBleachRois_Precalculated_Commander extends Job_Default{

	public static final String INPUT_IMG_TAG="Selection";
	//public static String FRAP_IMG_TAG="PA";
	
	
	
	
	
	public static final String KEY_TIME_DELAY="Time delay";

	public static final String KEY_REF_TABLE_PATH="Reference table path";
	public static final String KEY_POSITION_TAG = "Position identifier";
	public static final String KEY_ROIS_TAG="Region class";
	public static final String KEY_CORRECT_ROIS_TAG="Correct positions of ROIs";
	public static final String KEY_CORRECTION_CHANNEL_TAG="Correction channel";
	
	
	public static final String KEY_OBJECT_MAX_COUNT="Maximal number of obejcts to select";
	
	
	private ImagePlus inImg;
	private Roi[] identifiedObjectRois;
	private ZProjector maxProjector;

	
	private Roi[] bleachRois;
	int timeDelay=1000;

	private String refTablePath="";
	private String positionTag="PositionIndex";
	private String roiTag="PA.Regions";
	private boolean correctRois=true;
	private int correctionChannel=1;
	
	
	private int objectMaxCount=3;


	
	
	//String xRoisString;
	//String yRoisString;
	


	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleBleachRois_Precalculated_Commander(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		inImg=null;
		identifiedObjectRois=null;
		bleachRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		TimeUnit.MILLISECONDS.sleep(timeDelay);
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		//load reference table
		TableModel referenceTable=new TableModel(new File(refTablePath));
		
		this.showDebug(inImg, "original image", true);

		// get file name
		String imafeFileName=currentTable.getFileName(curDInd, INPUT_IMG_TAG, "IMG");
		
				
		// extract position index from the file name
		String fileNameRegex=".*--W[0-9]{4}--P([0-9]{4})-{1,2}T[0-9]{4}\\.czi";
		Pattern fileNamePattern=Pattern.compile(fileNameRegex);
		Matcher matcher=fileNamePattern.matcher(imafeFileName);
		int actualPosition=0;
		if (matcher.find()) {
			actualPosition=Integer.parseInt(matcher.group(1));
		}else {
			throw new RuntimeException("Position can not be extracted from fileName");
		}
		
		
		int referenceTableIndex=-1;
		for (int index=0; index<referenceTable.getRowCount(); index++) {
			if(referenceTable.getNumericValue(index, positionTag)==actualPosition) {
				referenceTableIndex=index;
				break;
			}
		}
		
		File roiFile=referenceTable.getFile(referenceTableIndex, roiTag, "ROI");
		
		//check if Roi File exists
		if (roiFile==null)
			return false;
		if (!roiFile.exists())
			return false;
		
		bleachRois=ROIManipulator2D.flsToRois(roiFile);
		
		if (bleachRois.length>objectMaxCount){
			int[] selectedIndexes=ArrIndUtils.getRandomIndexes(bleachRois.length, objectMaxCount);
			Roi[] selectedRois=new Roi[objectMaxCount];
			for (int i=0;i<objectMaxCount;i++)
				selectedRois[i]=bleachRois[selectedIndexes[i]];
			bleachRois=selectedRois;
		}
		
		if (!correctRois) {
			return true;
		}
		
		//correct positions of ROIs using correlation between 2 images
		
		
		
		
		////prepare reference image
		ImagePlus referenceImage=ImageOpenerWithBioformats.openImage(referenceTable.getFile(referenceTableIndex, "LowZoom"/*INPUT_IMG_TAG*/, "IMG"));
		if (referenceImage.getNChannels()>1) {
			referenceImage=new Duplicator().run(referenceImage, correctionChannel, correctionChannel, 1, referenceImage.getNSlices(), 1, referenceImage.getNFrames());
		}
		if (referenceImage.getNSlices()>1) {
			maxProjector=new ZProjector(referenceImage);
			maxProjector.setMethod(ZProjector.MAX_METHOD);
			maxProjector.doProjection();
			referenceImage=maxProjector.getProjection();
		}
		
		////prepare current image
		ImagePlus currentImage;
		if(inImg.getNChannels()>1) {
			currentImage=new Duplicator().run(inImg, correctionChannel, correctionChannel, 1, inImg.getNSlices(), 1, inImg.getNFrames());
		}else {
			currentImage=inImg;
		}
		if (currentImage.getNSlices()>1) {
			maxProjector=new ZProjector(currentImage);
			maxProjector.setMethod(ZProjector.MAX_METHOD);
			maxProjector.doProjection();
			currentImage=maxProjector.getProjection();
		}
		
		IJ.log("ROIs extracted");
		////calculate shift
		Point2D roiShift=computeShiftUsingPhaseCorrelation(referenceImage, currentImage);
		IJ.log("shift calculated");
		////update ROI positions
		for (Roi br:bleachRois) {
			Rectangle2D rect = br.getFloatBounds();
			br.setLocation(rect.getX()-roiShift.getX(), rect.getY()-roiShift.getY());
			//referenceImage.setRoi(bleachRois[iRoi]);
			//IJ.run(referenceImage,"Translate... ", String.format("x=%d y=%d",-Math.round(roiShift.getX()),-Math.round(roiShift.getY())));
			//bleachRois[iRoi]=referenceImage.getRoi();
		}
		IJ.log("shift applied");
		
		return true;
	}
	
	
	private Point2D computeShiftUsingPhaseCorrelation(ImagePlus imp1, ImagePlus imp0) {
        //_data.showDebugMessage("PhaseCorrelation phc = new PhaseCorrelation(...)");
		//if( _data.logger.isShowDebug() )   logger.info();
        PhaseCorrelation phc = new PhaseCorrelation(ImagePlusAdapter.wrap(imp1), ImagePlusAdapter.wrap(imp0), 5, true);
        //_data.showDebugMessage("phc.process()... ");
        //if( logger.isShowDebug() )   logger.info("phc.process()... ");
        phc.process();
        // get the first peak that is not a clean 1.0,
        // because 1.0 cross-correlation typically is an artifact of too much shift into black areas of both images
        ArrayList<PhaseCorrelationPeak> pcp = phc.getAllShifts();
        float ccPeak = 0;
        int iPeak = 0;
        for(iPeak = pcp.size()-1; iPeak>=0; iPeak--) {
            ccPeak = pcp.get(iPeak).getCrossCorrelationPeak();
            if (ccPeak < 0.999) break;
        }
        //info(""+ccPeak);
        int[] shift = pcp.get(iPeak).getPosition();
        return new Point2D.Double(shift[0],shift[1]);
    }



	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (bleachRois!=null)
			for (Roi r:bleachRois){
				r.setStrokeColor(Color.cyan);
				o.add(r);
			}
		if (identifiedObjectRois!=null)
			for (Roi r:identifiedObjectRois){
				r.setStrokeColor(Color.yellow);
				o.add(r);
			}
		
		return o;
		
	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		
		this.microscopeCommander.submitJobPixels(newImgFile, this.expTag, null, bleachRois);
		
		int nRois=bleachRois.length;
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", nRois));
		
		
		Roi[] overlayRoiArray=getOverlay().toArray();
		if(!ROIManipulator2D.isEmptyRoiArr(overlayRoiArray)) {
			saveRoisForImage(newImgFile, overlayRoiArray);
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		super.postProcessFail();
		this.microscopeCommander.submitJobPixels(newImgFile, "", null, new Roi[0]);

		//ZeissLSM800.submitJobPixels(newImgFile, null, null, null, null,selectedRoiManager.xPositions,selectedRoiManager.yPositions,selectedRoiManager.radiusValues);


		IJ.log("No region was selected. Continue with selection imaging job");
	}

	@Override
	public void postProcessOffline(){
		boolean selectionMade=!ROIManipulator2D.isEmptyRoiArr(bleachRois);
		IJ.log("Selection made: "+selectionMade);
		if (selectionMade)
			IJ.log("Number of selected bleach ROIs " + bleachRois.length);
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_TIME_DELAY, null, timeDelay, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_REF_TABLE_PATH, null, refTablePath, ParameterType.FILEPATH_PARAMETER);
		jobCollection.addParameter(KEY_POSITION_TAG, null, positionTag, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_ROIS_TAG, null, roiTag, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_CORRECT_ROIS_TAG, null, correctRois, ParameterType.BOOL_PARAMETER);
		jobCollection.addParameter(KEY_CORRECTION_CHANNEL_TAG, null, correctionChannel, ParameterType.INT_PARAMETER);
		

		
		jobCollection.addParameter(KEY_OBJECT_MAX_COUNT,null, objectMaxCount, ParameterType.INT_PARAMETER);
		
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.timeDelay=(Integer)_jobParameterCollection.getParameterValue(KEY_TIME_DELAY);
		
		this.refTablePath=(String)_jobParameterCollection.getParameterValue(KEY_REF_TABLE_PATH);
		this.positionTag=(String)_jobParameterCollection.getParameterValue(KEY_POSITION_TAG);
		this.roiTag=(String)_jobParameterCollection.getParameterValue(KEY_ROIS_TAG);
		this.correctRois=(Boolean)_jobParameterCollection.getParameterValue(KEY_CORRECT_ROIS_TAG);
		this.correctionChannel=(Integer)_jobParameterCollection.getParameterValue(KEY_CORRECTION_CHANNEL_TAG);

		
		this.objectMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_OBJECT_MAX_COUNT);
		
		if (!(new File(refTablePath)).isFile()) {
			throw new RuntimeException("Reference Table Path was not selected correctly. Pipeline will stop now.");
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)throws Exception{
		// start ImageJ
		ImageJ ij=new ImageJ();
		ij.ui().showUI();
		//scriptService=ij.script();
		//new ij.ImageJ();
		
		final String tblAPth="C:\\tempdat\\Terra\\Automated_Photoconversion_20230308\\summary_AutoMicTools.txt";
		final String imgFnm="Selection--W0000--P0001-T0001.czi";
		Job_GetMultipleBleachRois_Precalculated_Commander testJob=new Job_GetMultipleBleachRois_Precalculated_Commander();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
