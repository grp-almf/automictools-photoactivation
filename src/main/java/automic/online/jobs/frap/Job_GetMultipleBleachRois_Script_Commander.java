package automic.online.jobs.frap;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.ArrIndUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import loci.plugins.in.ImporterOptions;

import net.imagej.ImageJ;

public class Job_GetMultipleBleachRois_Script_Commander extends Job_Default{

	
	
	public static final String INPUT_IMG_TAG="Selection";
	//public static String FRAP_IMG_TAG="PA";
	
	public static final String KEY_TIME_DELAY="Time delay";

	public static final String KEY_SCRIPT_PATH="Script path";
	public static final String KEY_REGION_TYPE="Region type";
	
	
	public static final String KEY_BLEACH_RADIUS = "Bleach circle radius";
	
	public static final String KEY_OBJECT_MAX_COUNT="Maximal number of obejcts to select";
	
	//region keys
	public static final String CIRCLE_REGION="circle";
	public static final String POLYGON_REGION="polygon";
	
	
	private ImagePlus inImg;
	private Roi[] identifiedObjectRois;
	private Point2D.Double[] identifiedPoints;
	private Point2D.Double[] selectedPoints;

	private Roi[] bleachRois;
	int timeDelay=1000;

	private String scriptPath="";
	String regionType=CIRCLE_REGION;
	
	private int objectMaxCount=10;
	private int bleachRadius=40;


	
	
	//String xRoisString;
	//String yRoisString;
	


	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleBleachRois_Script_Commander(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		inImg=null;
		identifiedObjectRois=null;
		identifiedPoints=null;
		selectedPoints=null;
		bleachRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		TimeUnit.MILLISECONDS.sleep(timeDelay);
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		this.showDebug(inImg, "original image", true);

		ROIManipulator2D.getEmptyRm();
		inImg.show();
		
		ij2.script().run(new File(scriptPath), false,new HashMap<String,Object>()).get();
		
		inImg.hide();
		//final String macro = readFile( scriptPath );
		//System.out.println(macro);
		//new Interpreter().runBatchMacro( macro, inImg );
		Overlay segmentationOverlay=inImg.getOverlay();
		if (segmentationOverlay==null)
			return false;
		
		
		identifiedObjectRois=segmentationOverlay.toArray();
		if (identifiedObjectRois.length<1)
			return false;

		for (Roi r:identifiedObjectRois){
			r.setStrokeColor(Color.yellow);
			r.setPosition(0);
			r.setPosition(0,0,0);
		}
		

		switch(regionType) {
			case POLYGON_REGION: //react on left arrow Key
				if (identifiedObjectRois.length<=objectMaxCount) {
					bleachRois=identifiedObjectRois;
				}else {
					int[] selectedIndexes=ArrIndUtils.getRandomIndexes(identifiedObjectRois.length, objectMaxCount);
					bleachRois=new Roi[objectMaxCount];
					for (int i=0;i<objectMaxCount;i++)
						bleachRois[i]=identifiedObjectRois[selectedIndexes[i]];
				}
				break;

			case CIRCLE_REGION: //react on right arrow Key
				
				double x,y;
				identifiedPoints=new Point2D.Double[identifiedObjectRois.length];
				for(int iObject=0;iObject<identifiedObjectRois.length;iObject++) {
					Rectangle roiBounds=identifiedObjectRois[iObject].getBounds();
					x=roiBounds.getCenterX();
					y=roiBounds.getCenterY();
					identifiedPoints[iObject]=new Point2D.Double(x, y);
				}
				
				if (identifiedPoints.length<=objectMaxCount){
					selectedPoints=identifiedPoints;
				}
				else{
					int[] selectedIndexes=ArrIndUtils.getRandomIndexes(identifiedPoints.length, objectMaxCount);
					selectedPoints=new Point2D.Double[objectMaxCount];
					for (int i=0;i<objectMaxCount;i++)
						selectedPoints[i]=identifiedPoints[selectedIndexes[i]];
				}
				
				
				bleachRois=convertSelectedPointArrayToBleachRoiArray(selectedPoints);
				for (Roi r:bleachRois){
					r.setStrokeColor(Color.cyan);
					r.setPosition(0);
					r.setPosition(0,0,0);
				}
				break;

			
			default:throw new RuntimeException("Unknown region type");
		}
		
	
		return true;
	}
	
	private Roi convertSelectedPointToBleachRoi(Point2D.Double _selectedPoint) {
		

		double centerX=_selectedPoint.x;
		double centerY=_selectedPoint.y;
		
		Roi bleachRoi=new OvalRoi(centerX-bleachRadius, centerY-bleachRadius, 2*bleachRadius, 2*bleachRadius);
		
		bleachRoi.setStrokeColor(Color.pink);
		return bleachRoi;

	}
	
	private Roi[] convertSelectedPointArrayToBleachRoiArray(Point2D.Double[] _selectedPointArray) {
		Roi[] overlayRoiArray=new Roi[_selectedPointArray.length];
		for (int i=0;i<_selectedPointArray.length;i++) {
			overlayRoiArray[i]=convertSelectedPointToBleachRoi(_selectedPointArray[i]);
		}
		
		return overlayRoiArray;
	}

	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (identifiedObjectRois!=null)
			for (Roi r:identifiedObjectRois){
				o.add(r);
			}

		if (bleachRois!=null)
			for (Roi r:bleachRois){
				if (!o.contains(r)) o.add(r);
			}
		
		return o;
		
	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		
		this.microscopeCommander.submitJobPixels(newImgFile, "", null, bleachRois);
		
		int nRois=bleachRois.length;
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", nRois));
		
		
		Roi[] overlayRoiArray=getOverlay().toArray();
		if(!ROIManipulator2D.isEmptyRoiArr(overlayRoiArray)) {
			saveRoisForImage(newImgFile, overlayRoiArray);
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		super.postProcessFail();
		this.microscopeCommander.submitJobPixels(newImgFile, "", null, new Roi[0]);

		//ZeissLSM800.submitJobPixels(newImgFile, null, null, null, null,selectedRoiManager.xPositions,selectedRoiManager.yPositions,selectedRoiManager.radiusValues);


		IJ.log("No region was selected. Continue with selection imaging job");
	}

	@Override
	public void postProcessOffline(){
		boolean selectionMade=!ROIManipulator2D.isEmptyRoiArr(bleachRois);
		IJ.log("Selection made: "+selectionMade);
		if (selectionMade)
			IJ.log("Number of selected bleach ROIs " + bleachRois.length);
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_TIME_DELAY, null, timeDelay, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_SCRIPT_PATH, null, scriptPath, ParameterType.FILEPATH_PARAMETER);
		
		jobCollection.addParameter(KEY_OBJECT_MAX_COUNT,null, objectMaxCount, ParameterType.INT_PARAMETER);
		
		jobCollection.addSelectionParameter(KEY_REGION_TYPE, null, regionType, ParameterType.STRING_PARAMETER, new String[] {CIRCLE_REGION,POLYGON_REGION});
		
		jobCollection.addParameter(KEY_BLEACH_RADIUS, null, bleachRadius, ParameterType.INT_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.timeDelay=(Integer)_jobParameterCollection.getParameterValue(KEY_TIME_DELAY);
		
		this.scriptPath=(String)_jobParameterCollection.getParameterValue(KEY_SCRIPT_PATH);
		
		this.objectMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_OBJECT_MAX_COUNT);
		
		this.regionType=(String)_jobParameterCollection.getParameterValue(KEY_REGION_TYPE);
		
		this.bleachRadius=(Integer)_jobParameterCollection.getParameterValue(KEY_BLEACH_RADIUS);
	}
	
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)throws Exception{
		// start ImageJ
		ImageJ ij=new ImageJ();
		ij.ui().showUI();
		//scriptService=ij.script();
		//new ij.ImageJ();
		
		final String tblAPth="C:\\tempdat\\Terra\\Automated_Photoconversion_20230308\\summary_AutoMicTools.txt";
		final String imgFnm="Selection--W0000--P0001-T0001.czi";
		Job_GetMultipleBleachRois_Script_Commander testJob=new Job_GetMultipleBleachRois_Script_Commander();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
