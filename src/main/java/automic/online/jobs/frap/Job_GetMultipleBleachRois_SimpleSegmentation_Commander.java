package automic.online.jobs.frap;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;
import java.util.concurrent.TimeUnit;


import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.ArrIndUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.RankFilters;
import ij.plugin.frame.RoiManager;
import loci.plugins.in.ImporterOptions;

public class Job_GetMultipleBleachRois_SimpleSegmentation_Commander extends Job_Default{
	public static final String INPUT_IMG_TAG="Selection";
	//public static String FRAP_IMG_TAG="PA";
	
	
	
	
	
	public static final String KEY_TIME_DELAY="Time delay";
	public static final String KEY_SEGMENTATION_CHANNEL_INDEX="Segmentation channel";
	public static final String KEY_INTENSITY_THRESHOLD="Intensity Threshold";
	public static final String KEY_NUCLEUS_MIN_AREA="Minimal area";
	public static final String KEY_NUCLEUS_MAX_AREA="Maximal area";
	public static final String KEY_NUCLEUS_MIN_INTENSITY="Minimal intensity";
	public static final String KEY_NUCLEUS_MAX_INTENSITY="Maximal intensity";
	public static final String KEY_NUCLEUS_MIN_AR="Minimal aspect ratio";
	public static final String KEY_NUCLEUS_MAX_AR="Maximal aspect ratio";
	public static final String KEY_NUCLEUS_MIN_LONG_AXIS="Minimal long axis";
	public static final String KEY_NUCLEUS_MAX_LONG_AXIS="Maximal long axis";
	public static final String KEY_NUCLEUS_MIN_SHORT_AXIS="Minimal short axis";
	public static final String KEY_NUCLEUS_MAX_SHORT_AXIS="Maximal short axis";
	
	public static final String KEY_NUCLEI_MAX_COUNT="Maximal number of objects to select";
	public static final String KEY_BLEACH_RADIUS = "Bleach radius";
	
	
	private ImagePlus inImg;
	private Roi[] identifiedCellRois;
	private Roi[] filteredCellRois;
	private Roi[] selectedRois;
	private Roi[] bleachRois;
	int timeDelay=1000;

	
	private int segChannel=1;
	private int segThreshold=50;
	
	private int minSize=1000;
	private int maxSize=10000;
	private double minIntensity=100;
	private double maxIntensity=255;
	private double minAR=1.7;
	private double maxAR=20;
	private double minLongAxis=20;
	private double maxLongAxis=250;
	private double minShortAxis=10;
	private double maxShortAxis=50;
	
	private int nucMaxCount=3;
	
	
	private int bleachRadius=40;
	
	//String xRoisString;
	//String yRoisString;
	


	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleBleachRois_SimpleSegmentation_Commander(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		inImg=null;
		identifiedCellRois=null;
		filteredCellRois=null;
		selectedRois=null;
		bleachRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		TimeUnit.MILLISECONDS.sleep(timeDelay);
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		this.showDebug(inImg, "original image", true);

		ImagePlus segStackImage=new Duplicator().run(inImg, segChannel, segChannel, 1, inImg.getNSlices(), 1, 1);
		ZProjector maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		maxProjector.setImage(segStackImage);
		maxProjector.doProjection();
		ImagePlus maxProjectedImage=maxProjector.getProjection();
		
		segmentObjects(maxProjectedImage);
		if (filteredCellRois==null)	return false;
		if (filteredCellRois.length<1)	return false;

		if (filteredCellRois.length<=nucMaxCount){
			selectedRois=filteredCellRois;
		}
		else{
			int[] selectedIndexes=ArrIndUtils.getRandomIndexes(filteredCellRois.length, nucMaxCount);
			selectedRois=new Roi[nucMaxCount];
			for (int i=0;i<nucMaxCount;i++)
				selectedRois[i]=filteredCellRois[selectedIndexes[i]];
		}
		
		for (Roi r:selectedRois)
			r.setStrokeColor(Color.cyan);

		bleachRois=convertSelectedRoiArrayToBleachRoiArray(selectedRois);
		
		return true;
	}
	
	private void segmentObjects(ImagePlus _embryoImage)throws Exception {
		new RankFilters().rank(_embryoImage.getProcessor(), 3, RankFilters.MEDIAN);
		this.showDebug(_embryoImage, "Image for embryo segmentation", true);
		IJ.setThreshold(_embryoImage, segThreshold, Double.MAX_VALUE);
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		ParticleAnalyzer pAnalyzer=new ParticleAnalyzer(ParticleAnalyzer.ADD_TO_MANAGER|ParticleAnalyzer.SHOW_NONE|ParticleAnalyzer.FOUR_CONNECTED|ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES, 
														0,
														null,
														minSize, maxSize, 0.0, 1.0);
		
		pAnalyzer.analyze(_embryoImage);
		if (rm.getCount()<1)
			return;

		identifiedCellRois=rm.getRoisAsArray();
		for (Roi r:identifiedCellRois)
			r.setStrokeColor(Color.red);

		
		ParticleFilterer objectFilter=new ParticleFilterer(_embryoImage.getProcessor(), identifiedCellRois);
		objectFilter.filterThr(ParticleFilterer.MEAN, minIntensity, maxIntensity);
		objectFilter.filterThr(ParticleFilterer.ASPECT_RATIO, minAR, maxAR);
		objectFilter.filterThr(ParticleFilterer.LONG_AXIS, minLongAxis, maxLongAxis);
		objectFilter.filterThr(ParticleFilterer.SHORT_AXIS, minShortAxis, maxShortAxis);
		filteredCellRois=objectFilter.getPassedRois();
		
		if (filteredCellRois==null)
			return;
		for (Roi r:filteredCellRois)
			r.setStrokeColor(Color.yellow);
		
		return;
	}
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (selectedRois!=null)
			for (Roi r:selectedRois){
				o.add(r);
			}
		
		return o;
		
	}
	
	private Roi convertSelectedRoiToBleachRoi(Roi _selectedRoi) {
		

		Rectangle bounds=_selectedRoi.getBounds();
		double centerX=bounds.getX()+bounds.getWidth()/2;
		double centerY=bounds.getY()+bounds.getHeight()/2;
		
		Roi bleachRoi=new OvalRoi(centerX-bleachRadius, centerY-bleachRadius, 2*bleachRadius, 2*bleachRadius);
		
		bleachRoi.setStrokeColor(Color.pink);
		return bleachRoi;

	}
	
	private Roi[] convertSelectedRoiArrayToBleachRoiArray(Roi[] _selectedRoiArray) {
		Roi[] overlayRoiArray=new Roi[_selectedRoiArray.length];
		for (int i=0;i<_selectedRoiArray.length;i++) {
			overlayRoiArray[i]=convertSelectedRoiToBleachRoi(_selectedRoiArray[i]);
		}
		
		return overlayRoiArray;
	}

	
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		
		this.microscopeCommander.submitJobPixels(newImgFile, "", null, bleachRois);
		
		int nRois=selectedRois.length;
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", nRois));
		
		if (nRois>0) {
			saveRoisForImage(newImgFile, selectedRois);
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		super.postProcessFail();
		this.microscopeCommander.submitJobPixels(newImgFile, "", null, new Roi[0]);

		//ZeissLSM800.submitJobPixels(newImgFile, null, null, null, null,selectedRoiManager.xPositions,selectedRoiManager.yPositions,selectedRoiManager.radiusValues);


		IJ.log("No region was selected. Continue with selection imaging job");
	}

	@Override
	public void postProcessOffline(){
		boolean selectionMade=!ROIManipulator2D.isEmptyRoiArr(selectedRois);
		IJ.log("Selection made: "+selectionMade);
		if (selectionMade)
			IJ.log("Number of selected ROIs " + selectedRois.length);
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_TIME_DELAY, null, timeDelay, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_SEGMENTATION_CHANNEL_INDEX, null, segChannel, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_INTENSITY_THRESHOLD, null, segThreshold, ParameterType.INT_PARAMETER);

		jobCollection.addParameter(KEY_NUCLEUS_MIN_AREA, null, minSize, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_AREA, null, maxSize, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_INTENSITY, null, minIntensity, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_INTENSITY, null, maxIntensity, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_AR, null, minAR, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_AR, null, maxAR, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_LONG_AXIS, null, minLongAxis, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_LONG_AXIS, null, maxLongAxis, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_SHORT_AXIS, null, minShortAxis, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_SHORT_AXIS, null, maxShortAxis, ParameterType.DOUBLE_PARAMETER);
		
		jobCollection.addParameter(KEY_NUCLEI_MAX_COUNT,null, nucMaxCount, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_BLEACH_RADIUS, null, bleachRadius, ParameterType.INT_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.timeDelay=(Integer)_jobParameterCollection.getParameterValue(KEY_TIME_DELAY);
		
		this.segChannel=(Integer)_jobParameterCollection.getParameterValue(KEY_SEGMENTATION_CHANNEL_INDEX);
		this.segThreshold=(Integer)_jobParameterCollection.getParameterValue(KEY_INTENSITY_THRESHOLD);
		
		this.minSize=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_AREA);
		this.maxSize=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_AREA);
		this.minIntensity=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_INTENSITY);
		this.maxIntensity=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_INTENSITY);
		this.minAR=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_AR);
		this.maxAR=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_AR);
		this.minLongAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_LONG_AXIS);
		this.maxLongAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_LONG_AXIS);
		this.minShortAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_SHORT_AXIS);
		this.maxShortAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_SHORT_AXIS);
		
		this.nucMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEI_MAX_COUNT);
		
		this.bleachRadius=(Integer)_jobParameterCollection.getParameterValue(KEY_BLEACH_RADIUS);
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="X:\\group\\ALMFstuff\\Aliaksandr\\test_Feedback_LSM900_Sanjana\\20200204-084938\\summary_.txt";
		final String imgFnm="LowZoom--W0000--P0002-T0001.czi";
		Job_GetMultipleBleachRois_SimpleSegmentation_Commander testJob=new Job_GetMultipleBleachRois_SimpleSegmentation_Commander();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
