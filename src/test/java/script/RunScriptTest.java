package script;

import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;

import ij.macro.Interpreter;
import ij.plugin.frame.RoiManager;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class RunScriptTest {

	
	
	
	//static String scriptPath="C:/Halavatyi-work/Fiji-prog/joanna-zukowska-golgi-morphology/scripts/measure-golgi-features-online.ijm";

	static String scriptPath="C:\\Halavatyi-work\\Fiji-prog\\AutoMicTools\\script-and-macro-examples\\Segment_nuclei.ijm";
	
	static String testImagePath="C:/tempdat/DataForFeedbackMicroscopyWorkshop/Photoactivation-TestData-202105/Example-Photoactivation-ForSimulation/02-Selection--W0000--P0003-T0001.czi";
	
	
	public static void main(String[] args)throws Exception{

		System.getProperties().setProperty("plugins.dir", "C:\\Halavatyi-work\\running-soft\\Fiji.app-2021\\plugins");

		// start ImageJ
		new ImageJ();

		ImagePlus testImage=ImageOpenerWithBioformats.openImage(testImagePath);
		//IJ.run("Bio-Formats", "open="+testImagePath);
		//ImagePlus testImage=IJ.getImage();
		ROIManipulator2D.getEmptyRm();
		final String macro = readFile( scriptPath );
		System.out.println(macro);
		new Interpreter().runBatchMacro( macro, testImage );
		//rm.setVisible(true);
		//
		
		testImage.show();
	}

	private static String readFile( String filePath )
	   {
	      StringBuilder contentBuilder = new StringBuilder();

	      try ( Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
	      {
	         stream.forEach(s -> contentBuilder.append(s).append("\n"));
	      }
	      catch ( IOException e)
	      {
	         e.printStackTrace();
	      }

	      return contentBuilder.toString();
	   }

	
}
