package script;

import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
//import ij.ImageJ;
import ij.ImagePlus;
import net.imagej.ImageJ;

import java.io.File;

import org.apache.commons.collections15.map.HashedMap;
import org.scijava.Context;
import org.scijava.io.IOService;
import org.scijava.plugin.Parameter;
import org.scijava.script.DefaultScriptService;
import org.scijava.script.ScriptService;

public class RunScriptServiceTest {

	//@Parameter
	//private static ScriptService scriptService;
	
	

	static String scriptPath="C:\\Halavatyi-work\\Fiji-prog\\AutoMicTools\\script-and-macro-examples\\Segment_nuclei.ijm";
	
	static String testImagePath="C:/tempdat/DataForFeedbackMicroscopyWorkshop/Photoactivation-TestData-202105/Example-Photoactivation-ForSimulation/02-Selection--W0000--P0003-T0001.czi";
	
	
	public static void main(String[] args)throws Exception{

		//System.getProperties().setProperty("plugins.dir", "C:\\Halavatyi-work\\running-soft\\Fiji.app-2021\\plugins");
		//System.setProperty("imagej.legacy.sync", "true");
		
		//new net.imagej.ImageJ();

		// start ImageJ
		//new ImageJ();
		//new ImageJ();
		
		final ImageJ ij = new ImageJ();
		
        ij.ui().showUI();
        ScriptService scriptService=ij.script();
        //System.out.println(scriptService);

		ImagePlus testImage=ImageOpenerWithBioformats.openImage(testImagePath);
		testImage.show();
		

		ROIManipulator2D.getEmptyRm();

		//IJ.run("Bio-Formats", "open="+testImagePath);
		//ImagePlus testImage=IJ.getImage();
		
		//ScriptService scriptService=new DefaultScriptService();
		//scriptService.setContext(ij.getContext());
		
		scriptService.run(new File(scriptPath), false, new HashedMap<String, Object>()).get();
		
		//rm.setVisible(true);
		//
		
		//testImage.show();
		System.out.println("Done");
	}

	
}
