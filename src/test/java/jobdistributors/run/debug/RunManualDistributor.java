package jobdistributors.run.debug;

//import automic.online.jobdistributors.JobDistributor_MicroscopeCommander_AF_Photomanipulation_Script;
import automic.online.jobdistributors.JobDistributor_MicroscopeCommander_AF_Photomanipulation_Manual;
//import automic.online.jobdistributors.JobDistributor_MicroscopeCommander_AF_Photomanipulation_Segmentation;
//import automic.online.jobdistributors.JobDistributor_ZenBlue_AF_Photomanipulation_Manual;
import ij.IJ;
import ij.ImageJ;

public class RunManualDistributor {
	public static void main(String[] args) {
		Class<?> clazz = JobDistributor_MicroscopeCommander_AF_Photomanipulation_Manual.class;

		// start ImageJ
		new ImageJ();

		//IJ.runPlugIn(clazz.getName(),"Debug run");
		IJ.runPlugIn(clazz.getName(),"");
	}
}
