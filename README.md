# Adaptive Feedback Microscopy procedures for the automated photoactivation of specific cell phenotypes 

### General information

With this project we demonstrate how to test and run the pre-defined protocols for automated selective photoactivation of the specific cell phenotypes using [**AutoMicTools**](https://git.embl.de/halavaty/AutoMicTools) library. With our library you can also run pipelines covering other applications (e.g. high resolution imaging of transient or rare phenotypes or tracking moving samples) or even define your own pipelines for specific project needs. The global documentation covering different use cases is currently under construction. Please send us a message if you need the guidelines to try these tools for your application.

The protocol described below explains confuguing and testing the part of the protocol which runs from  **Fiji**. Depending on the microscope brand you would need to make addiditonal configuration in the microscope software. We currently support Zeiss microscope controlled by either ZenBlack or ZenBlue software and Olympus FluoView 3000 microscopes with Remote Development Kit (RDK) module. There is a possibility to extend the library for using with some other automated microscopes. Please approach us for the additional microscope-specific information.

The up-to-date version of this manual is accessible [here](https://git.embl.de/grp-almf/automictools-photoactivation).

### Authors

 These tools have been developed and are currently maintained at the Advanced Light Microscopy Facility (ALMF) of European Molecular Biology Laboratory (EMBL, Heidelberg, Germany) by Aliaksandr Halavatyi ([aliaksandr.halavatyi@embl.de](aliaksandr.halavatyi@embl.de)) and Manuel Gunkel ([manuel.gunkel@embl.de](manuel.gunkel@embl.de)). Please send us a message if you have a question or a feedback about using these tools. 


### Installing FIJI and AutoMic Tools

Please follow below guidelines to install **Fiji** and all the plugins that are required for the workflow.
1. Download [**Fiji**](https://imagej.net/software/fiji/downloads) matching your operating system and install it on your computer.
> Note: if you are a Windows user, there is no installation needed. You just need to download **Fiji** and unpack it in a certain destination. We recommend you to use the folder where you always have writing access on your computer (you do not need to use the ***Program Files*** folder).
This gives you a possibility to have multiple Fiji installations for different purposes  - you do not need to delete your old **Fiji** if you have one for other tasks, just install an additional one for the course.

2. Start **Fiji**.

3. Install additional **Fiji** plugins, that will be required for completing the online training material and during the course:
    - Run `Help -> Update...` from the main Menu
    - Press *Manage update sites*
    - Activate needed update sites from the default list (tick corresponding checkboxes in front of these items):
        - *3D ImageJ Suite*
        - *CSBDeep*
        - *IJPB-pluigns*
        - *ImageScience*
    - On the bottom of the list add URLs of required **Fiji** update sites, that are missing in the default list and tick corresponding checkboxes in front of these items:
        - *SegmentationAnnotator (https://sites.imagej.net/SegmentationAnnotator)
        - *AutoMic Tools (https://sites.imagej.net/AutoMicTools)*
        > Note: To add each URL you need to press [Add Update site] and in the line appearing on the bottom of the table type the corresponding URL in the URL column.

5. Close `Manage update sites` dialog

6. Click `Apply changes` and wait until new dependencies are downloaded

7. Restart **Fiji**.


### Simulating the acquisition and image processing

In order to test the image processing separately from a microscope you need a test dataset, which you can download [here](https://oc.embl.de/index.php/s/hW8T5NCDF4xsfG5). Now you can simulate the microscope acquisition process by copying one image after another to a specific folder that is watched by the AutoMicTools pipeline, in the sequence as they would be acquired by the microscope. In the following example we use the manual selection tool as image processing step:
1. create a folder where you will put the data for simulation. Pay attention that this folder is writable and the complete path does not contain spaces or special characters.
2. Start **Fiji** and then the pipeline of AutomicToos executing an ImageJ macro for object recognition by selecting `Plugins > Auto Mic Tools > Applications>Photomanipulation > AF LowZoom and PA - Script`.
A dialog `JobDistributorParameters` opens where you can specify the `Image File Extension` of the images you want to have processed, the `Experiment folder` that shold be watched for new files as well as the `Microscope Commander` which defines the communication methods with the respective microscope system:

 <img src=doc/AutoMicTools_dialog01.PNG alt="AutoMic Tools dialog 1" width="500"> 

This dialog is always shown for all AutoMic Tools pipelines.

3. In the next dialog you can set the pipeline specific paramaters, in this case you can specify the `Time delay` that specifies how many ms after initial creation of a file this file should be opened in order to avoid read/write conflicts, under `Script path` you can browwse to the macro you want to use for image processing, here count-golgi-fragments-online.ijm. In `X`- and `Y position column` you need to specify the coordinates of the recognized objects as they are saved in the respective columns in the Results table of the macro. Additionally you can set a `Maximal number of objects to select` and the `Bleach radius` in pixel of the circular ROI which will be used for photoactivation in this case:

 <img src=doc/AutoMicTools_dialog02.PNG alt="AutoMic Tools dialog 2" width="500"> 

4. Now AutoMic Tools is watching the specified folder for new images. You will see a new dialog as long as the folder specified is monitored:

<img src=doc/AutoMicTools_FileMonitor.PNG alt="file monitoring active" width="200"> 

Copy the images to that folder in the sequence the microscope would acquire them. On the selection images you can see the ROIs of the selected phenotypes in yellow as well as the circular areas selected for photoactivation in cyan:

<img src=doc/ROIselect.PNG alt="selected objects" width="750"> 

5. You can change the parameters of the macro in order to select cells with more fragmented golgi or filter nuclei based on their size by editing the values in the highlighted part of the macro:

<img src=doc/AutoMicTools-macro.PNG alt="editable parameters of the macro" width="1024"> 

5. At the end you can stop the file watching by pressing `OK` in the `file monitor` dialog. 


### Browsing datasets acquired with AutoMic Tools

1. Go to the `dataForBrowsing` folder in the example data set (this is the subset from a real experiment as it is actually saved during acquisition).
2. Open file experiment_summary.txt in Excel on any other spreadsheet software
3. Explore the structure of the table
4. For each image type to be annotated PathName_ And FileName_ values are specified
6. Start AutoMic Browser:
`Plugins › Auto Mic Tools › AutoMic Browser`
8. Select the summary_AutoMicTools.txt file as an input
9. Visualise images (tick corresponding checkboxes)
10. Visualise identified ROIs (Exp Rois checkboxes)
11. Check if photoactivated cells match selected ROIs.

